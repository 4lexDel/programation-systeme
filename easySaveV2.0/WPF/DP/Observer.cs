﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Threading;

namespace EasySave.DP
{
    public interface IObserver
    {
        void update(ISubject subject);        // Receive update from subject
    }

    public interface ISubject
    {
        void attach(IObserver observer);    // Attach an observer to the subject.
        void detach(IObserver observer);    // Detach an observer from the subject.
        void notify();  // Notify all observers about an event.
    } 
}

