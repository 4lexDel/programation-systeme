﻿using EasySave.Models;
using EasySave.Models.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySave.Views
{
    /// <summary>
    /// Logique d'interaction pour EasySaveView.xaml
    /// </summary>
    public partial class EasySaveView : Window
    {
        public EasySaveView()
        {
            InitializeComponent();
            ChangeLanguageModel.SetDefaultLanguage(this);

            foreach (MenuItem item in menuItemLanguages.Items)
            {
                if (item.Tag.ToString().Equals(ChangeLanguageModel.GetCurrentCultureName(this)))
                {
                    item.IsChecked = true;
                }
            }
        }
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            foreach (MenuItem item in menuItemLanguages.Items)
            {
                item.IsChecked = false;
            }

            MenuItem mi = sender as MenuItem;
            mi.IsChecked = true;
            ChangeLanguageModel.SwitchLanguage(this, mi.Tag.ToString());
        }


    }
}
