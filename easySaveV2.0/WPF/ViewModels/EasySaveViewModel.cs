﻿using EasySave.Models;
using EasySave.Models.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace EasySave.ViewModels
{
    class EasySaveViewModel : ViewModelBase
    {

        private string name;
        private string sourcePath;
        private string targetPath;
        private string businessSoftPath;
        private int saveMode;

        private string logFormat;
        private string encryptedExtension;
        private string priorityExtension;

        private SetDirectoryPathCommand setSourceDirectoryPath;
        private SetDirectoryPathCommand setTargetDirectoryPath;
        private SetBusinessSoftPathCommand setBusinessSoftPath;


        private CreateSaveCommand createSaveCommand;
        private SelectSaveCommand selectSaveCommand;

        private SingleInstanceModel singleInstanceModel;

        private UseSaveCommand useSaveCommand;

        private ObservableCollection<SaveModel> savesModel;

        private List<SaveModel> realTimeWorkSaves;
        private SaveModel realTimeWorkSaveSelected;

        private ThreadControlModel tc;

        public EasySaveViewModel()
        {
            //savesModel = new List<SaveModel>() { new SaveModel("first", "source", "target", 0), new SaveModel("second", "source", "target", 0) }; //List of works saves 
            savesModel = new ObservableCollection<SaveModel>(); //List of works saves 
            realTimeWorkSaves = new List<SaveModel>();

            setSourceDirectoryPath = new SetDirectoryPathCommand(this);
            setTargetDirectoryPath = new SetDirectoryPathCommand(this);
            setBusinessSoftPath = new SetBusinessSoftPathCommand(this);


            createSaveCommand = new CreateSaveCommand(this);
            selectSaveCommand = new SelectSaveCommand(this);

            useSaveCommand = new UseSaveCommand(this);


            tc = new ThreadControlModel();

            singleInstanceModel = new SingleInstanceModel();
            singleInstanceModel.SingleInstance();
        }

        public ThreadControlModel Tc
        {
            get { return tc; }

            set
            {
                tc = value;
            }
        }
        public string Name
        {
            get { return name; }

            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public string SourcePath
        {
            get { return sourcePath; }

            set
            {
                sourcePath = value;
                OnPropertyChanged("SourcePath");
            }
        }
        public string BusinessSoftPath
        {
            get { return businessSoftPath; }

            set
            {
                businessSoftPath = value;
                OnPropertyChanged("BusinessSoftPath");
            }
        }

        public string TargetPath
        {
            get { return targetPath; }

            set
            {
                targetPath = value;
                OnPropertyChanged("TargetPath");
            }
        }

        public int SaveMode
        {
            get { return saveMode; }

            set
            {
                saveMode = value;
                OnPropertyChanged("SaveMode");
            }
        }

        public ObservableCollection<SaveModel> SavesModel 
        {
            get {
                return savesModel;
            }
            set
            {
                savesModel = value;
                OnPropertyChanged("SavesModel");
            }
        }

        public List<SaveModel> RealTimeWorkSaves
        {
            get
            {
                return realTimeWorkSaves;
            }
            set
            {
                realTimeWorkSaves = value;
                OnPropertyChanged("RealTimeWorkSaves");
            }
        }

        public SaveModel RealTimeWorkSaveSelected
        {
            get
            {
                return realTimeWorkSaveSelected;
            }
            set
            {
                realTimeWorkSaveSelected = value;
                OnPropertyChanged("RealTimeWorkSaveSelected");
            }
        }

        public string EncryptedExtension
        {
            get
            {
                return encryptedExtension;
            }

            set
            {
                encryptedExtension = value;
                OnPropertyChanged("EncryptedExtension");
            }
        }

        public string PriorityExtension
        {
            get
            {
                return priorityExtension;
            }

            set
            {
                priorityExtension = value;
                OnPropertyChanged("PriorityExtension");
            }
        }

        public string LogFormat
        {
            get { return logFormat; }

            set
            {
                logFormat = value;
                OnPropertyChanged("LogFormat");
            }
        }

        public ICommand SetSourceDirectoryPath
        {
            get
            {
                return setSourceDirectoryPath;
            }
        }

        public ICommand SetTargetDirectoryPath
        {
            get
            {
                return setTargetDirectoryPath;
            }
        }
        public ICommand SetBusinessSoftPath
        {
            get
            {
                return setBusinessSoftPath;
            }
        }

        public ICommand CreateSaveCommand
        {
            get
            {
                return createSaveCommand;
            }
        }

        public ICommand SelectSaveCommand
        {
            get
            {
                return selectSaveCommand;
            }
        }

        public ICommand UseSaveCommand
        {
            get
            {
                return useSaveCommand;
            }
        }
    }
}
