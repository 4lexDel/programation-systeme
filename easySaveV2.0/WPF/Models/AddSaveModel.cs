﻿using EasySave.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.Json;

namespace EasySave.Models
{
    class AddSaveModel
    {
        private EasySaveViewModel viewModel;

        public AddSaveModel(EasySaveViewModel viewModel)
        {
            this.viewModel = viewModel;


        }

        public void WritingJson()
        {
            // Set the file path where the JSON data will be saved
            string filePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\ExistingWorkSave.json";

            // Create the file if it does not exist
            if (!File.Exists(filePath))
            {
                var newFile = File.Create(filePath);
                newFile.Close();
            }

            // Create a new WorkSave object with the entered information
            var workSave = new WorkSave
            {
                Name = viewModel.Name,
                SourcePath = viewModel.SourcePath,
                TargetPath = viewModel.TargetPath,
                SaveMode = viewModel.SaveMode
            };

            // Read the existing JSON data from the file
            var json = File.ReadAllText(filePath);
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };

            // Deserialize the JSON string to an array of WorkSave objects
            var workSaves = new WorkSave[0];
            if (!string.IsNullOrWhiteSpace(json))
            {
                workSaves = JsonSerializer.Deserialize<WorkSave[]>(json, options);
            }

            // Add the new workSave to the array
            Array.Resize(ref workSaves, workSaves.Length + 1);
            workSaves[workSaves.Length - 1] = workSave;

            // Serialize the array back to JSON and write it to the file
            json = JsonSerializer.Serialize(workSaves, options);
            File.WriteAllText(filePath, json);        }

    }

    // Define a class to represent the information about a work being saved
    public class WorkSave
    {
        public string Name { get; set; }
        public string SourcePath { get; set; }
        public string TargetPath { get; set; }
        public int SaveMode { get; set; }
    }
}
