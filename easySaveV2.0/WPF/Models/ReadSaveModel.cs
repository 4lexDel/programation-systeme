﻿using EasySave.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.Json;

namespace EasySave.Models
{
    class ReadSaveModel
    {
        public ReadSaveModel()
        {
        }

        public WorkSaved[] ReadingJson()
        {
            // Set the file path
            string filePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\ExistingWorkSave.json";

            // Check if the file exists
            if (!File.Exists(filePath))
            {
                return null;
            }

            // Loop until the user quits

            // Read the JSON file
            using (StreamReader sr = new StreamReader(filePath))
            {
                string json = sr.ReadToEnd();
                var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
                WorkSaved[] workSaves = JsonSerializer.Deserialize<WorkSaved[]>(json, options);

                return workSaves;
            }
        }

    }

    // Define a class to represent the information about a work being saved
    public class WorkSaved
    {
        public string Name { get; set; }
        public string SourcePath { get; set; }
        public string TargetPath { get; set; }
        public int SaveMode { get; set; }
    }
}
