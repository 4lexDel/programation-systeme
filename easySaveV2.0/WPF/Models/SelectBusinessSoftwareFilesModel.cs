﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EasySave.Models
{
    class SelectBusinessSoftwareFilesModel
    {

        public bool SelectBusinessSoftwareFiles(string appName)
        {
            Process[] processes = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(appName));
            if (processes.Length == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
