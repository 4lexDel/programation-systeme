﻿using System;
using System.Text.Json;
using System.IO;
using System.Windows;

namespace EasySave.Models
{
    public class InstantLogModel
    {

        // Object for print info in instant log file
        public class Data
        {
            public string Name { get; set; }
            public string FileSource { get; set; }
            public string FileTarget { get; set; }
            public bool State { get; set; }
            public int TotalFilesToCopy { get; set; }
            public long TotalFilesSize { get; set; }
            public int NbFilesLeftToDo { get; set; }
            public long Progression { get; set; }
            public string Time { get; set; }
        }

        private string LogSavePath = string.Empty;

        // function for writting log in correct emplacement
        public void LogWrite(string name, string fileSource, string fileTarget, bool state, int totalFilesToCopy, long totalFilesSize, int nbFilesLeftToDo, long totalFilesSizeLeftToDo, string formatMode)
        {
            try
            {
                long progression = Progression(totalFilesSizeLeftToDo, totalFilesSize);
                if (formatMode == "1")
                {
                    LogSavePath = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\logInstantFinal.xml";
                    LogXML(name, fileSource, fileTarget, state, totalFilesToCopy, totalFilesSize, nbFilesLeftToDo, progression, LogSavePath);
                }
                else
                {
                    LogSavePath = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\logInstantFinal.json";
                    //Save into the user directory
                    using (StreamWriter logFileInstant = File.AppendText(LogSavePath))
                    {
                        LogJSON(name, fileSource, fileTarget, state, totalFilesToCopy, totalFilesSize, nbFilesLeftToDo, progression, logFileInstant);
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Save Error");
                Console.WriteLine(ex);
            }
        }

        // function for writting log in .json format 
        public void LogJSON(string name, string fileSource, string fileTarget, bool state, int totalFilesToCopy, long totalFilesSize, int nbFilesLeftToDo, long progression, TextWriter txtWriter)
        {
            try
            {
                Data data = new Data() { Name = name, FileSource = fileSource, FileTarget = fileTarget, State = state, TotalFilesToCopy = totalFilesToCopy, TotalFilesSize = totalFilesSize, NbFilesLeftToDo = nbFilesLeftToDo, Progression = progression, Time = (DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToShortDateString()) };
                string json = JsonSerializer.Serialize(data); // api for format object in .json
                txtWriter.Write(json + ',' + '\n'); //Write the informations in the logs files
            }
            catch (Exception ex)
            {
                Console.WriteLine("JSON Log Error");
                Console.WriteLine(ex);
            }
        }

        // function for writting log in .xml format 
        public void LogXML(string name, string fileSource, string fileTarget, bool state, int totalFilesToCopy, long totalFileSyze, int nbFilesLeftToDo, long progression, string LogSavePath)
        {
            try
            {
                string Time = (DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToShortDateString());
                Data overview = new Data
                {
                    Name = name,
                    FileSource = fileSource,
                    FileTarget = fileTarget,
                    State = state,
                    TotalFilesToCopy = totalFilesToCopy,
                    TotalFilesSize = totalFileSyze,
                    NbFilesLeftToDo = nbFilesLeftToDo,
                    Progression = progression,
                    Time = Time
                };

                System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(Data));
                TextWriter xml = new StreamWriter(LogSavePath, true);
                writer.Serialize(xml, overview);
                xml.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("XML Log Error");
                Console.WriteLine(ex);
            }
        }



        // function for update progression during the process
        public long Progression(long a, long b)
        {
            if (b == 0)
            {
                return 100;
            }
            else
            {
                return (100 - a * 100 / b);
            }

        }
    }
}
