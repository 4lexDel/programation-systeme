﻿using EasySave.Utils;
using EasySave.ViewModels;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Windows;

namespace EasySave.Models
{
    class ServerModel
    {
        private IPEndPoint ipEndPoint;
        private List<Socket> clients;

        private EasySaveViewModel viewModel;

        public ServerModel(string ipAdress, int port, EasySaveViewModel viewModel)
        {
            ipEndPoint = new IPEndPoint(IPAddress.Parse(ipAdress), port);

            clients = new List<Socket>();

            this.viewModel = viewModel;
        }

        public List<Socket> Clients {       //To secure clients manipulations
            get
            {
                lock(clients)
                { 
                    return clients;
                }
            }
            set
            {
                lock(clients)
                {
                    clients = value;
                }
            }
        }

        public void run()
        {
            try
            {
                //MessageBox.Show("Hello Server!");

                Socket listener = createServerSocket();

                while (true)
                {
                    Console.WriteLine("Waiting for new connection ... ");

                    Socket clientSocket = acceptClient(listener);
                    Clients.Add(clientSocket);

                    broadcast("<VAL>New client");

                    Thread thread = new Thread(() => {
                        Console.WriteLine("New client connected !");
                        while (true)
                        {
                            try
                            {
                                if (clientCommunication(clientSocket) == -1)
                                {
                                    Clients.Remove(clientSocket);
                                    break;
                                }

                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                clients.Remove(clientSocket);
                                break;
                            }
                        }
                        Console.WriteLine("end");
                    });
                    thread.Start();
                }
            }
            catch (Exception)
            {
            //    throw;
            }
        }

        private Socket createServerSocket()
        {
            Socket socketServer = new Socket(ipEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            socketServer.Bind(ipEndPoint);
            socketServer.Listen(10);

            return socketServer;
        }

        private Socket acceptClient(Socket socket)
        {
            return socket.Accept();
        }

        private int clientCommunication(Socket client)
        {
            byte[] bytes = new Byte[1024];
            string data = null;

            while (true)
            {
                int numByte = client.Receive(bytes);

                if (numByte == 0)
                {
                    return -1;
                }

                data += Encoding.ASCII.GetString(bytes, 0, numByte);
                if (data.IndexOf("<EOF>") > -1)
                    break;
            }
            Console.WriteLine("Text reçu -> {0} ", data);

            if(data.Substring(0, 5) == "<ACT>")
            {
                string action = data.Substring(5);

                if(action == "pause<EOF>"){
                    //MessageBox.Show("Server : Pause");
                    viewModel.Tc.Running = false;
                }
                else if (action == "stop<EOF>")
                {
                    //MessageBox.Show("Server : Stop");
                    viewModel.Tc.Stopped = true;
                }
                else if (action == "play<EOF>")
                {
                    //MessageBox.Show("Server : Play");
                    viewModel.Tc.Running = true;
                }
            }
            

            //string message = null;

            //byte[] messageByte = Encoding.ASCII.GetBytes(message);
            //client.Send(messageByte);

            return 0;
        }

        public void broadcast(string message)
        {
            //Console.WriteLine("Broadcast!");
            foreach (Socket client in Clients)
            {
                client.Send(Encoding.ASCII.GetBytes(message));
            }
        }

        public void broadcastWorkSave<T>(T workSave)
        {
            //byte[] dataToSend = ByteArraySerializer<T>.Serialize(workSave);
            ////Console.WriteLine("Broadcast!");
            //foreach (Socket client in Clients)
            //{
            //    client.Send(dataToSend);
            //}

            string jsonObj = JsonSerializer.Serialize(workSave)+"#";    //Permet de dissocier chaque insatance en cas de chevauchement

            //MessageBox.Show("Server : "+jsonObj);

            byte[] data = Encoding.ASCII.GetBytes(jsonObj);

            foreach (Socket client in Clients)
            {
                client.Send(data);
            }
        }

        private void disconnect(Socket socket)
        {
            //fermer la connexion
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }
    }
}
