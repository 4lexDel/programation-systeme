﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.Json;

namespace EasySave.Models
{
    public class DailyLogModel
    {

        // Object for print info in instant log file
        public class Data
        {
            public string Name { get; set; }
            public string DirectorySource { get; set; }
            public string DirectoryTarget { get; set; }
            public long DirectorySize { get; set; }
            public string DirectoryTransferTime { get; set; }
            public string TimeCrypt { get; set; }
            public string Time { get; set; }
        }

        private string LogSavePath = string.Empty;

        // function for writting log in correct emplacement
        public void LogWrite(string name, string directorySource, string directoryTarget, long directorySize, string directoryTransferTime, string typeCrypt, string formatMode)
        {
            try
            {
                if (formatMode == "1")
                {
                    LogSavePath = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\log " + DateTime.Now.ToString("MM-dd-yyyy") + ".xml";
                    LogXML(name, directorySource, directoryTarget, directorySize, directoryTransferTime,typeCrypt, LogSavePath);
                }
                else
                {
                    LogSavePath = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\log " + DateTime.Now.ToString("MM-dd-yyyy") + ".json";
                    using (StreamWriter logFile = File.AppendText(LogSavePath))
                    {
                        LogJSON(name, directorySource, directoryTarget, directorySize, directoryTransferTime,typeCrypt, logFile);
                    }
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine("Save Error");
                Console.WriteLine(ex);
            }
        }

        //Convert all the element in a JSON format and write it in the logs files
        public void LogJSON(string name, string directorySource, string directoryTarget, long directorySize, string directoryTransferTime,string timeCrypt, TextWriter txtWriter)
        {
            try
            {
                string Time = (DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToShortDateString());
                Data data = new Data() { Name = name, DirectorySource = directorySource, DirectoryTarget = directoryTarget, DirectorySize = directorySize, DirectoryTransferTime = directoryTransferTime,TimeCrypt = timeCrypt  , Time = Time };
                string json = JsonSerializer.Serialize(data); //Converts the values into a JSON string.
                txtWriter.Write(json + ',' + '\n'); //Write the informations in the logs files
            }
            catch (Exception ex)
            {
                Console.WriteLine("JSON Log Error");
                Console.WriteLine(ex);
            }
        }

        //Convert all the element in a XML format and write it in the logs file
        public void LogXML(string name, string directorySource, string directoryTarget, long directorySize, string directoryTransferTime,string timeCrypt, string LogSavePath)
        {
            try
            {
                string Time = (DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToShortDateString());
                Data overview = new Data
                {
                    Name = name,
                    DirectorySource = directorySource,
                    DirectoryTarget = directoryTarget,
                    DirectorySize = directorySize,
                    DirectoryTransferTime = directoryTransferTime,
                    TimeCrypt = timeCrypt,
                    Time = Time
                };

                System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(Data));
                TextWriter xml = new StreamWriter(LogSavePath, true);
                writer.Serialize(xml, overview);
                xml.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("XML Log Error");
                Console.WriteLine(ex);
            }
        }
    }
}