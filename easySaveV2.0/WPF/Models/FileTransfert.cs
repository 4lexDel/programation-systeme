﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace EasySave.Models
{
    class FileTransfert
    {
        private FileInfo source;
        private FileInfo target;

        public FileTransfert(FileInfo source, FileInfo target)
        {
            this.source = source;
            this.target = target;
        }

        public FileInfo Source { get => source; set => source = value; }
        public FileInfo Target { get => target; set => target = value; }
    }
}
