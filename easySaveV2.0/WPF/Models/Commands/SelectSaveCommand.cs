﻿using EasySave.ViewModels;
using EasySave.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace EasySave.Models.Commands
{
    class SelectSaveCommand : ICommand
    {
        private EasySaveViewModel viewModel;
        public event EventHandler CanExecuteChanged;

        private ReadSaveModel readSaveModel;

        public SelectSaveCommand(EasySaveViewModel viewModel)
        {
            this.viewModel = viewModel;

            readSaveModel = new ReadSaveModel();
        }


        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            SelectWorkSaveView view = new SelectWorkSaveView();

            ObservableCollection<SaveModel> saveModelsStored = new ObservableCollection<SaveModel>();

            WorkSaved[] workSaves = readSaveModel.ReadingJson();

            if (workSaves != null)
            {
                foreach (WorkSaved item in workSaves)
                {
                    saveModelsStored.Add(new SaveModel(item.Name, item.SourcePath, item.TargetPath, item.SaveMode));
                }

                /*--------------------------------------------------------------------------------*/
                //                                    READ DATA
                /*--------------------------------------------------------------------------------*/

                viewModel.SavesModel = saveModelsStored;

                view.listViewSave.ItemsSource = viewModel.SavesModel;   //Refresh collection

                RoutedEventHandler p = (sender, e) => view.Close();
                view.selectSaveButton.Click += p;

                view.ShowDialog();

                ObservableCollection<SaveModel> workSavesSelected = new ObservableCollection<SaveModel>();

                foreach (var item in view.listViewSave.SelectedItems)
                {
                    SaveModel workSave = item as SaveModel;

                    if (workSave != null)
                    {
                        workSavesSelected.Add(workSave);
                    }
                }
                viewModel.SavesModel = workSavesSelected;
            }
            else
            {
                MessageBox.Show("No work save stored");
            }
        }
    }
}
