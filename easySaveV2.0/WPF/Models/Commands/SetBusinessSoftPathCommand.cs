﻿using EasySave.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace EasySave.Models.Commands
{
    class SetBusinessSoftPathCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        private EasySaveViewModel viewModel;


        public SetBusinessSoftPathCommand(EasySaveViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter != null)
            {
                string value = parameter.ToString();

                string folderPath = "";
                OpenFileDialog folderBrowserDialog1 = new OpenFileDialog();
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    folderPath = folderBrowserDialog1.FileName;
                    viewModel.BusinessSoftPath = folderPath;
                }
            }
        }
    }
}
