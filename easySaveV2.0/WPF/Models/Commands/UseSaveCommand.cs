﻿using EasySave.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using EasySave.DP;
using System.Windows;
using EasySave.Views;
using System.Collections.ObjectModel;
using System.Windows.Threading;

namespace EasySave.Models.Commands
{
    class UseSaveCommand : ICommand, IObserver
    {
        private EasySaveViewModel viewModel;
        private DailyLogModel dailyLogModel;
        private InstantLogModel instantLogModel;
        private RealTimeAdvancementView view;

        SelectBusinessSoftwareFilesModel BusinessSoftFiles = new SelectBusinessSoftwareFilesModel();

        ServerModel serverModel;

        public UseSaveCommand(EasySaveViewModel viewModel)
        {
            this.viewModel = viewModel;
            dailyLogModel = new DailyLogModel();
            instantLogModel = new InstantLogModel();

            serverModel = new ServerModel("127.0.0.1", 5000, viewModel);

            Thread serverThread = new Thread(serverModel.run);
            serverThread.Start();                               //          Thread decicate to manage the server
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            string files = viewModel.BusinessSoftPath;
            if (BusinessSoftFiles.SelectBusinessSoftwareFiles(files) == false)
            {
                MessageBox.Show("Business Software Detected / Application Métier détectée");
                return;
            }
            if (viewModel.SavesModel.Count > 0)
            {
                viewModel.Tc.ResetBooleanVar();
                initAvancementView();
                Thread checkProcessThread = new Thread(() =>
                {
                    CheckBusinessSoftwareFiles(files);
                });
                checkProcessThread.Start();
                foreach (SaveModel workSave in viewModel.SavesModel)
                {
                    Thread thread = new Thread(() =>
                    {
                        workSave.detach(this);  //TO AVOID PROBLEMS... (doublon)
                            workSave.attach(this);

                        string encryptedExtension = viewModel.EncryptedExtension;
                        encryptedExtension = encryptedExtension == null ? "" : encryptedExtension;  //Sécu (pas de valeur null)
                            string priorityExtension = viewModel.PriorityExtension;
                        priorityExtension = priorityExtension == null ? "" : priorityExtension;

                        workSave.process(encryptedExtension, priorityExtension, viewModel.Tc);
                        createDailyLog(workSave);
                    });
                    thread.Start();
                }
            }
            else
            {
                MessageBox.Show("No save work selected...");
            }

        }

        public void initAvancementView()
        {
            view = new RealTimeAdvancementView();

            viewModel.RealTimeWorkSaves.Clear();

            view.listViewSave.ItemsSource = viewModel.RealTimeWorkSaves;   //Refresh collection

            view.Show();

            view.listViewSave.Items.Refresh();
        }

        private void CheckBusinessSoftwareFiles(string Files)
        {
            while (true)
            {
                Thread.Sleep(500);
                bool isOpen = BusinessSoftFiles.SelectBusinessSoftwareFiles(Files);
                if (isOpen == false)
                {
                    viewModel.Tc.Running = false;
                }
            }
        }
        public void refreshWorkSaveAdvancement(SaveModel saveToRefresh)
        {
            try
            {
                App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (SendOrPostCallback)delegate//utilise le thread parent ? https://codes-sources.commentcamarche.net/forum/affich-1585132-c-wpf-probleme-de-threads / https://learn.microsoft.com/fr-fr/dotnet/api/system.windows.threading.dispatcher.verifyaccess?view=windowsdesktop-7.0
                {
                    bool exist = false;

                    foreach (SaveModel save in viewModel.RealTimeWorkSaves)
                    {
                        //MessageBox.Show(save.Status);
                        if (save.Equals(saveToRefresh))
                        {
                            exist = true;
                            break;
                        }
                    }
                    if (!exist)
                    {
                        viewModel.RealTimeWorkSaves.Add(saveToRefresh);
                    }

                    viewModel.RealTimeWorkSaves = viewModel.RealTimeWorkSaves;    //To call onPropertyChanged ??????

                    view.listViewSave.ItemsSource = viewModel.RealTimeWorkSaves;   //refresh
                    view.playButton.Click += PlayButton;
                    view.pauseButton.Click += PauseButton;
                    view.stopButton.Click += StopButton;
                    view.listViewSave.Items.Refresh();
                }, null);
            }
            catch (Exception)
            {
            }
        }

        public void sendWorkSaveToRemoteAccess(SaveModel saveToRefresh)
        {
            serverModel.broadcastWorkSave<WorkSaveModel>(saveToRefresh.convertToWorkSaveModel());
        }

        private void PlayButton(object sender, RoutedEventArgs e)
        {
            viewModel.Tc.Running = true;
        }
        private void PauseButton(object sender, RoutedEventArgs e)
        {
            viewModel.Tc.Running = false;
        }
        private void StopButton(object sender, RoutedEventArgs e)
        {
            viewModel.Tc.Stopped = true;
        }

        public void createDailyLog(SaveModel saveModel)
        {
            string name = saveModel.Name;
            string directorySource = saveModel.SourceDirectoryPath;
            string directoryTarget = saveModel.TargetDirectoryPath;
            long directorySize = saveModel.TotalLength;
            string directoryTransferTime = saveModel.TimeExec;
            string logFormat = viewModel.LogFormat;
            string timeCrypt = saveModel.TimeEncrypt + "ms";
            dailyLogModel.LogWrite(name, directorySource, directoryTarget, directorySize, directoryTransferTime, timeCrypt, logFormat);
        }

        public void update(ISubject subject)
        {
            SaveModel saveModel = subject as SaveModel;

            if (saveModel != null)
            {
                //Thread.Sleep(1000);
                refreshWorkSaveAdvancement(saveModel);
                sendWorkSaveToRemoteAccess(saveModel);  //For remote access
                createInstantLog(saveModel);

                //serverModel.broadcast("<VAL>Data from : " + saveModel.Name);
            }
        }

        public void createInstantLog(SaveModel saveModel)
        {
            string name = saveModel.Name;
            string fileSource = saveModel.CurrentSourceFilePath;
            string fileTarget = saveModel.CurrentTargetFilePath;
            bool state = saveModel.SaveState;
            int totalFilesToCopy = saveModel.NbFiles;
            long totalFilesSize = saveModel.TotalLength;
            long totalFilesSizeLeftToDo = saveModel.LengthFilesRemaining;
            int nbFilesLeftToDo = saveModel.NbFilesRemaining;
            string logFormat = viewModel.LogFormat;
            instantLogModel.LogWrite(name, fileSource, fileTarget, state, totalFilesToCopy, totalFilesSize, nbFilesLeftToDo, totalFilesSizeLeftToDo, logFormat);
        }
    }
}
