﻿using EasySave.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace EasySave.Models.Commands
{
    class CreateSaveCommand : ICommand
    {
        private EasySaveViewModel viewModel;

        private AddSaveModel addSaveModel;

        private ReadSaveModel readSaveModel;

        public CreateSaveCommand(EasySaveViewModel viewModel)
        {
            this.viewModel = viewModel;

            addSaveModel = new AddSaveModel(viewModel);

            readSaveModel = new ReadSaveModel();
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            //MessageBox.Show(viewModel.Name + "\n" + viewModel.SourcePath + "\n" + viewModel.TargetPath + "\n" + viewModel.SaveMode);

            SaveModel saveModel = new SaveModel(viewModel.Name, viewModel.SourcePath, viewModel.TargetPath, viewModel.SaveMode);
            //saveModel.attach(this);        //Observer attach !

            viewModel.SavesModel.Add(saveModel);

            AddSaveModel addSaveModel = new AddSaveModel(viewModel);
            addSaveModel.WritingJson();

            //ReadSaveModel readSaveModel = new ReadSaveModel(viewModel);
            //readSaveModel.ReadingJson();
            /////////
        }
    }
}
