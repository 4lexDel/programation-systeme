﻿using EasySave.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace EasySave.Models.Commands
{
    class SetDirectoryPathCommand : ICommand
    {
        private EasySaveViewModel viewModel;

        public SetDirectoryPathCommand(EasySaveViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter != null)
            {
                string value = parameter.ToString();

                string folderPath = "";
                FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    folderPath = folderBrowserDialog1.SelectedPath;

                    if (value == "source")
                    {
                        viewModel.SourcePath = folderPath;
                    }
                    else if (value == "target")
                    {
                        viewModel.TargetPath = folderPath;
                    }
                }               
            }
        }
    }
}
