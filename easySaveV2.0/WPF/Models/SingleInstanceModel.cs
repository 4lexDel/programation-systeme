﻿using EasySave.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Diagnostics;


namespace EasySave.Models
{
    class SingleInstanceModel
    {
        public void SingleInstance()
        {
            var EasySaveProgramm = Process.GetProcessesByName("EasySave").Single();
            var id = EasySaveProgramm.Id;

            Process[] processes = { Process.GetProcessById(id) };
            if (processes.Length > 1)
            {
                App.Current.Shutdown();
            }

        }

    }
}