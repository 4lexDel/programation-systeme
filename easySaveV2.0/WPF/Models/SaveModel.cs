﻿using EasySave.DP;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;

namespace EasySave.Models
{
    class SaveModel : ISubject
    {
        private List<IObserver> observers = new List<IObserver>();

        public static int FULL_MODE = 0;
        public static int DIFF_MODE = 1;
        public static int increment = 0;

        public delegate void MeasureTimeExecVoidFunc();

        private int saveID;

        private string name;
        private string sourceDirectoryPath;
        private string targetDirectoryPath;
        private int saveMode;

        private long totalLength = 0;//Total size
        private int nbFiles = 0;   //Nb files total
        private string timeExec; //Milliseconds
        private int timeEncrypt; //Milliseconds
        private bool saveState; //save finish

        private int nbFilesRemaining;   //Temps réel
        private long lengthFilesRemaining;
        private string currentSourceFilePath;
        private string currentTargetFilePath;
        private string keyPass;
        private string advancement;
        private ThreadControlModel tc;

        public int NbFilesRemaining { get => nbFilesRemaining; set => nbFilesRemaining = value; }
        public long LengthFilesRemaining { get => lengthFilesRemaining; set => lengthFilesRemaining = value; }
        public string CurrentSourceFilePath { get => currentSourceFilePath; set => currentSourceFilePath = value; }
        public string CurrentTargetFilePath { get => currentTargetFilePath; set => currentTargetFilePath = value; }
        public string Name { get => name; set => name = value; }
        public bool SaveState { get => saveState; set => saveState = value; }
        public int NbFiles { get => nbFiles; set => nbFiles = value; }
        public long TotalLength { get => totalLength; set => totalLength = value; }

        public string SourceDirectoryPath { get => sourceDirectoryPath; set => sourceDirectoryPath = value; }
        public string TargetDirectoryPath { get => targetDirectoryPath; set => targetDirectoryPath = value; }
        public string TimeExec { get => timeExec; set => timeExec = value; }
        public int TimeEncrypt { get => timeEncrypt; set => timeEncrypt = value; }
        public int SaveMode { get => saveMode; set => saveMode = value; }
        public string Status { get => status; set => status = value; }
        public string Advancement
        {
            get
            {
                return advancement;
            }
            set => advancement = value;
        }

        public int SaveID { get => saveID; set => saveID = value; }

        private List<FileTransfert> filesToCopy;
        private List<DirectoryInfo> directoriesToCreate;

        private string[] priorityExtensions;

        private string status;

        public SaveModel(string name, string sourcePath, string targetPath, int saveMode)
        {
            increment++;

            saveID = increment;

            this.name = name;
            this.sourceDirectoryPath = sourcePath;
            this.targetDirectoryPath = targetPath;
            this.saveMode = saveMode;

            filesToCopy = new List<FileTransfert>();
            directoriesToCreate = new List<DirectoryInfo>();

            observers = new List<IObserver>();

            saveState = false; // save is not finish
            status = "Pause";

            advancement = "0%";
        }

        public void process(string encryptedExtension, string priorityExtension, ThreadControlModel tc)
        {
            priorityExtensions = priorityExtension.Split(';');
            this.tc = tc;
            try
            {
                lengthFilesRemaining = 0;   //Reset
                nbFilesRemaining = 0;
                totalLength = 0;
                nbFiles = 0;
                timeEncrypt = 0;

                timeExec = measureTimeExec(() =>//This method mesurate the time exec of a method with an input delegate
                {
                    filesToCopy.Clear();
                    directoriesToCreate.Clear();
                    if (saveMode == FULL_MODE) refreshFullDirectoryInfo(new DirectoryInfo(sourceDirectoryPath), new DirectoryInfo(targetDirectoryPath));
                    else if (saveMode == DIFF_MODE) refreshDifferentialDirectoryInfo(new DirectoryInfo(sourceDirectoryPath), new DirectoryInfo(targetDirectoryPath));

                    #region affichage
                    Console.WriteLine("\nRECAP : " + name);
                    Console.WriteLine("Taille total : " + totalLength);
                    Console.WriteLine("Nombre de fichiers : " + nbFiles + "\n");
                    #endregion

                    save(encryptedExtension);
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                Console.WriteLine(ex);
            }
        }

        public void refreshFullDirectoryInfo(DirectoryInfo source, DirectoryInfo target)
        {
            if (!Directory.Exists(source.FullName)) return;

            directoriesToCreate.Add(target);

            foreach (FileInfo fileInfo in source.GetFiles())
            {
                nbFiles++;
                totalLength += fileInfo.Length;

                string targetFilePath = Path.Combine(target.ToString(), fileInfo.Name);
                FileInfo targetFile = new FileInfo(targetFilePath);

                bool priority = false;
                foreach (string extension in priorityExtensions)
                {
                    if (fileInfo.Extension == extension)
                    {
                        filesToCopy.Insert(0, new FileTransfert(fileInfo, targetFile));
                        priority = true;
                        break;
                    }
                }

                if (!priority) filesToCopy.Add(new FileTransfert(fileInfo, targetFile));
            }

            foreach (DirectoryInfo directoryInfo in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir = new DirectoryInfo(Path.Combine(target.ToString(), directoryInfo.Name));

                refreshFullDirectoryInfo(directoryInfo, nextTargetSubDir);
            }

            lengthFilesRemaining = totalLength;//Default
            nbFilesRemaining = nbFiles;
        }

        public void refreshDifferentialDirectoryInfo(DirectoryInfo source, DirectoryInfo target)
        {
            if (!Directory.Exists(source.FullName)) return;

            bool directoryExist = true;
            if (!Directory.Exists(target.FullName))
            {
                directoryExist = false;
                directoriesToCreate.Add(target);
            }

            foreach (FileInfo fileInfo in source.GetFiles())
            {
                string targetFilePath = Path.Combine(target.ToString(), fileInfo.Name);

                FileInfo previousTargetFile = new FileInfo(targetFilePath);

                if (!directoryExist || !previousTargetFile.Exists || !isSameFile(targetFilePath, fileInfo.FullName))
                {
                    nbFiles++;
                    totalLength += fileInfo.Length;

                    bool priority = false;
                    foreach (string extension in priorityExtensions)
                    {
                        if (fileInfo.Extension == extension)
                        {
                            filesToCopy.Insert(0, new FileTransfert(fileInfo, previousTargetFile));
                            priority = true;
                            break;
                        }
                    }

                    if (!priority) filesToCopy.Add(new FileTransfert(fileInfo, previousTargetFile));
                }
            }

            foreach (DirectoryInfo directoryInfo in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir = new DirectoryInfo(Path.Combine(target.ToString(), directoryInfo.Name));

                refreshDifferentialDirectoryInfo(directoryInfo, nextTargetSubDir);
            }

            lengthFilesRemaining = totalLength;//Default
            nbFilesRemaining = nbFiles;
        }

        public void save(string encryptedExtension)
        {
            status = "Play";

            foreach (DirectoryInfo directory in directoriesToCreate)
            {
                Directory.CreateDirectory(directory.FullName);
            }




            string fileListName = "";
            foreach (FileTransfert fileTransfert in filesToCopy)
            {
                if (tc.Stopped)
                {
                    status = "Stop";
                    lock (tc.Lock)
                    {
                        Monitor.Wait(tc.Lock);
                    }
                    break;
                }
                status = "Pause";
                while (!tc.Running) {
                }
                    status = "Play";
                    fileListName += fileTransfert.Source.Name + "\n";

                    string[] extensionToEncrypts = encryptedExtension.Split(';');

                    bool encrypted = false;

                    foreach (var extensionToEncrypt in extensionToEncrypts)
                    {
                        if (fileTransfert.Source.Extension == extensionToEncrypt)
                        {
                            encrypt(fileTransfert.Source.FullName, fileTransfert.Target.FullName, "../../Conf/Encrypt/key.txt");
                            encrypted = true;
                            break;
                        }
                    }

                    if (!encrypted) fileTransfert.Source.CopyTo(fileTransfert.Target.FullName, true);

                    nbFilesRemaining--;
                    lengthFilesRemaining -= fileTransfert.Source.Length;

                    refreshAvancement();

                    currentSourceFilePath = fileTransfert.Source.FullName;
                    currentTargetFilePath = fileTransfert.Target.FullName;

                    if (nbFilesRemaining <= 0)
                    {
                        saveState = true;
                        tc.Running = false;
                        status = "Finish";
                    }
                    notify();
               
            }

        }

        public void encrypt(string sourceFilesPath, string targetFilesPath, string keyPass)
        {
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "../../CryptoSoft/CryptoSoft.exe",
                    Arguments = '"' + sourceFilesPath + "\" " + '"' + targetFilesPath + '"' + " \"" + keyPass + '"',
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };
            process.Start();
            process.WaitForExit();
            timeEncrypt += process.ExitCode;
        }

        public string measureTimeExec(MeasureTimeExecVoidFunc func)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            func();
            stopWatch.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopWatch.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = string.Format("{0:00}h:{1:00}min:{2:00}s.{3:00}ms", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime + "\n");

            return elapsedTime;
        }

        public static bool isSameFile(string pathFile1, string pathFile2)
        {
            FileInfo f1 = new FileInfo(pathFile1);
            FileInfo f2 = new FileInfo(pathFile2);

            return f1.LastWriteTime == f2.LastWriteTime;
        }

        public void attach(IObserver observer)
        {
            observers.Add(observer);
        }

        public void detach(IObserver observer)
        {
            observers.Remove(observer);
        }

        public void notify()
        {
            foreach (IObserver observer in observers)
            {
                observer.update(this);
            }
        }

        public void refreshAvancement()
        {
            if (totalLength == 0) advancement = "100%";

            long value = 100 - (100 * lengthFilesRemaining / totalLength);
            advancement = value + "%";
        }

        public WorkSaveModel convertToWorkSaveModel()
        {
            return new WorkSaveModel(saveID, name, advancement, status);
        }
    }
}
