﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Windows;

namespace EasySave.Models
{
    //public delegate void SimpleDelg();
    class ThreadControlModel
    {
        private Thread _thread;
        private volatile bool _running = true;
        private volatile bool _stopped = false;


        private object _lock = new object();

        public bool Stopped {
            get
            {
                return _stopped;
            }
            set => _stopped = value; 
        }

        public bool Running { get => _running; set => _running = value; }
        public object Lock { get => _lock; set => _lock = value; }

        // private SimpleDelg delg;
        /*
        public ThreadControlModel(SimpleDelg delg)
        {
            this.delg = delg;
        }
        */



        /*
        public void Start()
        {
            _running = true;
            _stopped = false;

            _thread = new Thread(()=> { });
            _thread.Start();
        }*/

        public void Stop()
        {
            
            _stopped = true;
            _running = false;
            lock (_lock)
            {
                Monitor.Pulse(_lock);
            }
        }

        public void Suspend()
        {
            
            _running = false;
        }

        public void Resume()
        {
           
            _running = true;
            lock (_lock)
            {
                Monitor.Pulse(_lock);
            }
        }

        public void Join()
        {
            _stopped = false;
            //_thread.Join();
        }

        public void ResetBooleanVar()
        {
            _running = true;
            _stopped = false;
        }

    }
}
