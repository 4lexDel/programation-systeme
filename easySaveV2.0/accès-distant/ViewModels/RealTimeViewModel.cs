﻿using remoteAccess.Models;
using remoteAccess.Models.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace remoteAccess.ViewModels
{
    class RealTimeViewModel : ViewModelBase
    {
        private string ipAdress;
        private string port;

        ConnectionCommand connectionCommand;

        List<WorkSaveModel> workSaveModels;

        public RealTimeViewModel()
        {
            workSaveModels = new List<WorkSaveModel>();
            connectionCommand = new ConnectionCommand(this);
        }

        public List<WorkSaveModel> WorkSaveModels
        {
            get
            {
                return workSaveModels;
            }
            set
            {
                workSaveModels = value;
                OnPropertyChanged("WorkSaveModels");
            }
        }

        public string IpAdress
        {
            get
            {
                return ipAdress;
            }
            set
            {
                ipAdress = value;
                OnPropertyChanged("IpAdress");
            }
        }

        public string Port
        {
            get
            {
                return port;
            }
            set
            {
                port = value;
                OnPropertyChanged("Port");
            }
        }

        public ICommand ConnectionCommand
        {
            get
            {
                return connectionCommand;
            }
        }
    }
}
