﻿using System;
using System.Collections.Generic;
using System.Text;

namespace remoteAccess.Models
{
    class WorkSaveModel
    {
        private int id;
        private string name;
        private string advancement;
        private string status;

        public WorkSaveModel()
        {
        }

        public WorkSaveModel(int id, string name, string advancement, string status)
        {
            this.id = id;
            this.name = name;
            this.advancement = advancement;
            this.status = status;
        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Advancement { get => advancement; set => advancement = value; }
        public string Status { get => status; set => status = value; }
    }
}
