﻿using remoteAccess.Utils;
using remoteAccess.ViewModels;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace remoteAccess.Models
{
    class ClientModel
    {
        private IPEndPoint ipEndPoint;

        private Socket client;

        private RealTimeViewModel viewModel;
        private MainWindow view;

        public ClientModel(string ipAdress, int port, RealTimeViewModel viewModel, MainWindow view)
        {
            this.viewModel = viewModel;
            this.view = view;

            ipEndPoint = new IPEndPoint(IPAddress.Parse(ipAdress), port);

            init();
        }

        private void init()
        {
            try
            {
                //Console.WriteLine("Hello Client!");
                client = connectClient();

                MessageBox.Show("Socket connecté à -> " + client.RemoteEndPoint.ToString());

                //Console.WriteLine("Socket connecté à -> {0}", client.RemoteEndPoint.ToString());

                Thread listenServerThread = new Thread(() =>
                {
                    while (true)
                    {
                        try
                        {
                            listenServer(client);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    }
                });
                listenServerThread.Start();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
        }

        private Socket connectClient()
        {
            Socket newSocket = new Socket(ipEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            newSocket.Connect(ipEndPoint);

            return newSocket;
        }

        public void callServer(string input)
        {
            if (input.ToLower() == "exit")
            {
                Console.WriteLine("Disconnect");
                Thread.Sleep(200);
                disconnect(client);
                Environment.Exit(0);
            }

            if (client != null)
            {
                byte[] message = Encoding.ASCII.GetBytes(input + "<EOF>");
                int byteSent = client.Send(message);
            }
        }

        private void listenServer(Socket client)
        {
            byte[] messageReceived = new byte[1024];
            int byteRecv = client.Receive(messageReceived);

            string data = Encoding.ASCII.GetString(messageReceived, 0, byteRecv);

            //Console.WriteLine("Message du Serveur -> {0}", data);
            //MessageBox.Show("Client : receive");

            if (data.Substring(0, 5) == "<VAL>")
            {
                //MessageBox.Show(data.Substring(5));
                string result = data.Substring(5);

                MessageBox.Show(result);
            }
            else
            {
                //MessageBox.Show("Client : " + data);
                //Save !
                //WorkSaveModel input = ByteArraySerializer<WorkSaveModel>.Deserialize(messageReceived);

                string[] jsons = data.Split("#");

                foreach (string item in jsons)
                {
                    if (item.Trim() != string.Empty)
                    {
                        WorkSaveModel input = JsonSerializer.Deserialize<WorkSaveModel>(item);

                        if (input != null)
                        {
                            //MessageBox.Show("Id : " + input.Id + "\nName : " + input.Name + "\nAdvancement : " + input.Advancement + "\nStatus : " + input.Status);
                            refreshWorkSaveAdvancement(input);
                        }
                    }
                }
            }

        }

        public void refreshWorkSaveAdvancement(WorkSaveModel saveToRefresh)
        {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (SendOrPostCallback)delegate//utilise le thread parent ? https://codes-sources.commentcamarche.net/forum/affich-1585132-c-wpf-probleme-de-threads / https://learn.microsoft.com/fr-fr/dotnet/api/system.windows.threading.dispatcher.verifyaccess?view=windowsdesktop-7.0
            {
                bool exist = false;

                foreach (WorkSaveModel save in viewModel.WorkSaveModels)
                {
                    //MessageBox.Show(save.Status);
                    if (save.Id == saveToRefresh.Id)
                    {
                        exist = true;
                        viewModel.WorkSaveModels.Remove(save);      //On remove l'ancien
                        break;
                    }
                }
                    viewModel.WorkSaveModels.Add(saveToRefresh);

                //viewModel.RealTimeWorkSaves = viewModel.RealTimeWorkSaves;    //To call onPropertyChanged ??????

                view.listViewSave.ItemsSource = viewModel.WorkSaveModels;   //refresh
                view.listViewSave.Items.Refresh();
            }, null);
        }

        private void disconnect(Socket socket)
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }
    }
}
