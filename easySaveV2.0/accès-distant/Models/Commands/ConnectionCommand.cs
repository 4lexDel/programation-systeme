﻿using remoteAccess.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;

namespace remoteAccess.Models.Commands
{
    class ConnectionCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private RealTimeViewModel viewModel;
        private ClientModel clientModel;

        public ConnectionCommand(RealTimeViewModel viewModel)
        {
            this.viewModel = viewModel;
        }


        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            int portValue = -1;

            if(Int32.TryParse(viewModel.Port, out portValue))
            {
                MainWindow view = new MainWindow();

                view.listViewSave.ItemsSource = viewModel.WorkSaveModels;
                view.listViewSave.Items.Refresh();

                Thread threadClient = new Thread(() =>
                {
                    clientModel = new ClientModel(viewModel.IpAdress, portValue, viewModel, view);
                });
                threadClient.Start();

                view.playButton.Click += PlayButton_Click;
                view.pauseButton.Click += PauseButton_Click;
                view.stopButton.Click += StopButton_Click;

                view.ShowDialog();
            }

        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            clientModel.callServer("<ACT>stop");
        }

        private void PauseButton_Click(object sender, RoutedEventArgs e)
        {
            clientModel.callServer("<ACT>pause");
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            clientModel.callServer("<ACT>play");
        }
    }
}
